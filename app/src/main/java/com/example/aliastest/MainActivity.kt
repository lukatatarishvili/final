package com.example.aliastest

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth= FirebaseAuth.getInstance()

        init()
    }





    private fun init() {

            gilaki.setOnClickListener {
                if (Password.text.isNotEmpty() && Email.text.isNotEmpty()){
                    auth.signInWithEmailAndPassword(Email.text.toString(), Password.text.toString())
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                Toast.makeText(this, "წარმატებული ავტორიზაცია", Toast.LENGTH_LONG).show()
                                startActivity(Intent(this,Game::class.java))
                            }
                            else {
                                Toast.makeText(this, "ლოგინი ან პაროლი არასწორია", Toast.LENGTH_LONG).show()
                            }
                        }
                }else{
                    Toast.makeText(this, "შეავსე ველები", Toast.LENGTH_LONG).show()

                }





        }
    }






}
